import { Given,When, Then } from "cypress-cucumber-preprocessor/steps";
const url = 'http://pendullum.herokuapp.com'


Given('I open pendullum login page', () => {
    cy.visit(url)
    cy.get('.card-body > [href="/login"]').click()
})
When(`I put {string} and password {string} and log in.`, (username, password) => {
    cy.get('#email').type(username)
    cy.get('#password').type(password)
    cy.get('[data-testid="sign-in"]').click()
})
When('I open a raport form and fill it with a data - Line {string}, Direction {string}, Vehicle code {string}, Clostest stop {string}, Description{string}.',
(line, direction, code, stop, description) => {
    cy.get('.col-md-6 > [href="/post"]').click()
    cy.get('#line', { timeout: 10000 }).should('be.visible');
    cy.get('#line').select(line)
    cy.get('#direction').select(direction)
    cy.get('#code').type(code)
    cy.get('#stop').type(stop)
    cy.get('#description').type(description)
})

Then(`I am able to post a raport`,() => {
    cy.get('form > .my-3').click()
    ///cy.get(':nth-child(2) > .card-header > .row > :nth-child(2) > .btn').should('be.visible')
})