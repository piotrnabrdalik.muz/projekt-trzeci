import { Given, Then } from "cypress-cucumber-preprocessor/steps";
const url = 'http://pendullum.herokuapp.com'

Given('I open Pendullum page', () => {
    cy.visit(url)
})

Then(`I see {string} in the title`, (title) => {
    cy.get('main.py-3').should('be.visible')
    cy.get('h1').should('contain',title)
})
