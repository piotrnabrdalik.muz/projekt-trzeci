import { Given,When, Then } from "cypress-cucumber-preprocessor/steps";
const url = 'http://pendullum.herokuapp.com'


Given('I open Pendullum login page', () => {
    cy.visit(url)
    cy.get('.card-body > [href="/login"]').click()
})
When(`I put {string} and password {string} and log in.`, (username, password) => {
    cy.get('#email').type(username)
    cy.get('#password').type(password)
    cy.get('[data-testid="sign-in"]').click()
})
Then(`I am on the posting page`,() => {
    cy.visit(url+"/post")
    cy.get('#username').should("be.visible")
})